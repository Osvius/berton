<?php
require_once '../protected/config.php';

function validate($data, $to_check){
    $validate = array();
    if(!empty($data['new_pass']) && !empty($data['repeat_pass'])){
        if($data['new_pass'] != $data['repeat_pass']) {
            $validate['error'] = 'Пароли не совпадают!';
        }
    } else {
        $validate['error'] = 'Заполните все поля!';
    }
    if(isset($data['old_pass'])){
        if(md5($data['old_pass']) != $to_check['admin_pass']) {
            $validate['error'] = 'Неправильный пароль!';
        }
    } else {
        $validate['error'] = 'Заполните все поля!';
    }
    return $validate;
}

if(!isset($_SESSION['admin'])) {
    header('Location: ' . ADMIN_URL . '/login.php');
} else {
    $admin_info = getAdmin($db_connect);
    $to_check = array();

    foreach ($admin_info as $admin) {
        $to_check[$admin['setting']] = $admin['value'];
    }
    $success ='';

    if(isset($_POST['main'])) {
        $error = validate($_POST['main'], $to_check);
        if (empty($error)) {
            setAdminPass($db_connect, $_POST['main']);
            $success = 'Настройки сохранены успешно!';
        }
    }
    ?>
    <?php include_once 'header.php'; ?>
    <body>
        <div id="wrapper" class="toggled">
            <?php include_once 'sidebar.php'; ?>
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                                <form class="form-horizontal" method="post">
                                    <h3>Изменить пароль</h3>
                                    <div class="error">
                                        <?php echo (!empty($error))?$error['error']:$success; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Старый пароль:</label>
                                        <div class="col-sm-2">
                                            <input type="password" name="main[old_pass]" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Новый пароль:</label>
                                        <div class="col-sm-2">
                                            <input type="password" name="main[new_pass]" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Повторите новый пароль:</label>
                                        <div class="col-sm-2">
                                            <input type="password" name="main[repeat_pass]" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
    </body>
    <?php include_once 'footer.php'; ?>
<?php } ?>