        <!-- Sidebar -->
        <?php
        $path = $_SERVER['PHP_SELF'];
        $file = substr($path, strlen('/admin/'), strlen($path));
        ?>
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <p>Панель администратора</p>
                </li>
                <li>
                    <a <?php echo ($file == 'index.php')?'class="active"':''; ?> href="<?php echo ADMIN_URL; ?>">Основные настройки</a>
                </li>
                <li>
                    <a <?php echo ($file == 'pages.php')?'class="active"':''; ?> href="<?php echo ADMIN_URL .'/pages.php'; ?>">Страницы</a>
                </li>
                <li>
                    <a <?php echo ($file == 'product_list.php' || $file == 'product.php')?'class="active"':''; ?> href="<?php echo ADMIN_URL .'/product_list.php'; ?>">Товары</a>
                </li>
                <li>
                    <a href="<?php echo ADMIN_URL . '/logout.php'; ?>">Выход</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
