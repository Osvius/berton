<?php
include '../protected/config.php';
$login = '';

if(!empty($_POST['login']['admin_name'])){
    $login = $_POST['login']['admin_name'];
}

$admin_info = getAdmin($db_connect);
$to_check = array();

foreach ($admin_info as $admin) {
    $to_check[$admin['setting']] = $admin['value'];
}

$error = validate($_POST['login'], $to_check);
if(isset($_POST['login']) && empty($error)) {
    $_SESSION['admin'] = 'admin';
    header('Location: ' . ADMIN_URL );
}

function validate($data, $to_check){
    $validate = array();
        if (isset($data['admin_pass'])) {
            if(md5($data['admin_pass']) != $to_check['admin_pass']) {
                $validate['error'] = 'Неправильный пароль';
            }
        } else {
            $validate['error'] = 'Заполните все поля!';
        }
        if (isset($data['admin_name'])) {
            if($data['admin_name'] != $to_check['admin_name']) {
                $validate['error'] = 'Неправильный логин!';
            }
        } else {
            $validate['error'] = 'Заполните все поля';
        }

    return $validate;
}
?>
<?php include_once 'header.php'; ?>
<body>
    <div class="container">
        <form class="form-signin" method="post" name="login_form">
            <h2 class="form-signin-heading">Вход в админ-панель</h2>
            <div class="error">
                <?php
                echo (isset($error['error'])?$error['error']:'');
                ?>
            </div>
            <label for="inputName" class="sr-only">Логин</label>
            <input type="text" name="login[admin_name]" value="<?= $login ?>" id="inputName" class="form-control" placeholder="Логин" required autofocus>
            <label for="inputPassword" class="sr-only">Пароль</label>
            <input type="password" name="login[admin_pass]" id="inputPassword" class="form-control" placeholder="Пароль" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
        </form>

    </div> <!-- /container -->
</body>
<?php include_once 'footer.php'; ?>