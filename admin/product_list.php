<?php
require_once '../protected/config.php';
if(isset($_GET['delete_id'])) {
    deleteProduct($db_connect, $_GET['delete_id']);
    header('Location: ' . ADMIN_URL . '/product_list.php');
}
$products = getProducts($db_connect);
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<?php include_once 'header.php'; ?>
<body>
    <div id="wrapper" class="toggled">
    <?php include_once 'sidebar.php'; ?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Список товаров</h3>
                        <a href="<?= ADMIN_URL . '/product.php'; ?>"><button class="btn-primary">Добавить новый</button></a>
                        <table class="table-bordered table table-striped col-lg-8">
                            <thead>
                            <tr>
                                <td class="col-lg-4">Название</td>
                                <td>Артикул</td>
                                <td>Страница</td>
                                <td>Действие</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($products as $product) { ?>
                            <tr>
                                <td class="col-lg-4"><?= $product['rus_title']; ?></td>
                                <td><?= $product['code']; ?></td>
                                <td><?= $product['title']; ?></td>
                                <td>
                                    <a href="<?= ADMIN_URL . '/product.php?product_id='.$product['product_id']; ?>"><button class="btn-info btn-sm">Редактировать</button></a>
                                    <a href="<?= ADMIN_URL . '/product_list.php?delete_id='.$product['product_id']; ?>"><button href="#" class="btn-danger btn-sm">Удалить</button></a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include_once 'footer.php'; ?>

