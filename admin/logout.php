<?php
require_once '../protected/config.php';

unset($_SESSION['admin']);
header('Location: ' . ADMIN_URL . '/login.php');
