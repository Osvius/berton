<?php
require_once '../protected/config.php';

if(isset($_GET['product_id'])) {
    $product = getProduct($db_connect, $_GET['product_id']);
    if(isset($_POST['product'])) {
        $product = $_POST['product'];

        if(!empty($_FILES['product']['name']['image'])) {
            $path = DIR_IMG . '/products/'; // директория для загрузки
            $ext = array_pop(explode('.', $_FILES['product']['name']['image'])); // расширение
            $new_name = time() . '.' . $ext; // новое имя с расширением
            $full_path = $path . $new_name; // полный путь с новым именем и расширением

            if ($_FILES['image']['error'] == 0) {
                if (move_uploaded_file($_FILES['product']['tmp_name']['image'], $full_path)) {
                    $product['image'] = $new_name;
                }
            }
        } else {
            $product['image'] = $_POST['product']['image_old'];
        }
        updateProduct($db_connect, $_GET['product_id'], $product);
        header('Location: ' . ADMIN_URL . '/product_list.php');
    }
} else {
    if(isset($_POST['product'])) {
        $product = $_POST['product'];

        $path = DIR_IMG . '/products/'; // директория для загрузки
        $ext = array_pop(explode('.',$_FILES['product']['name']['image'])); // расширение
        $new_name = time().'.'.$ext; // новое имя с расширением
        $full_path = $path.$new_name; // полный путь с новым именем и расширением

        if($_FILES['image']['error'] == 0){
            if(move_uploaded_file($_FILES['product']['tmp_name']['image'], $full_path)){
                $product['image'] = $new_name;
            }
        }

        setProduct($db_connect, $product);
        header('Location: ' . ADMIN_URL . '/product_list.php');
    }
}
$colors = array(
    array('class' => '0', 'name' => 'Без цвета'),
    array('class' => 'yellow', 'name' => 'Желтый'),
    array('class' => 'red', 'name' => 'Красный'),
    array('class' => 'green', 'name' => 'Зеленый'),
);

$pages = getPages($db_connect);
?>
<?php include_once 'header.php'; ?>
<body xmlns="http://www.w3.org/1999/html">
    <div id="wrapper" class="toggled">
    <?php include_once 'sidebar.php'; ?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Карточка товара</h3>
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="error">
                                    <?php echo (!empty($error))?$error['error']:''; ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Название (на русском):</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="product[rus_title]" class="form-control" value="<?= @$product['rus_title']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Название (на английском):</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="product[eng_title]" class="form-control" value="<?= @$product['eng_title']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Артикул:</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="product[code]" class="form-control" value="<?= @$product['code']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Изображение:</label>
                                    <div class="col-sm-6">
                                        <img src="<?= SITE_URL . '/images/products/' . @$product['image']; ?>" alt="Загрузите картинку">
                                        <input type="file" name="product[image]" class="form-control"/>
                                        <input type="hidden" name="product[image_old]" value="<?= @$product['image']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Цвет (на фоне названия):</label>
                                    <div class="col-sm-6">
                                        <select name="product[color]">
                                            <?php foreach ($colors as $color) { ?>
                                                <option value="<?= $color['class']; ?>" <?php echo (@$product['color'] == $color['class'])?'selected':'';?> ><?= $color['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Отображать на странице:</label>
                                    <div class="col-sm-6">
                                        <select name="product[page_id]">
                                            <?php foreach ($pages as $page) { ?>
                                                <option value="<?= $page['page_id']; ?>" <?php echo (@$product['page_id'] == $page['page_id'])?'selected':'';?> ><?= $page['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Описание:</label>
                                    <div class="col-sm-6">
                                        <textarea cols="20" rows="10" name="product[product_description]" class="form-control"><?= @$product['product_description']; ?></textarea>
                                    </div>
                                </div>
                                </br>
                            <h2>Дополнительно</h2>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Температура:</label>
                                <div class="col-sm-6">
                                    <input type="text" name="product[temperature]" class="form-control" value="<?= @$product['temperature']; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Время заварки:</label>
                                <div class="col-sm-6">
                                    <input type="text" name="product[time]" class="form-control" value="<?= @$product['time']; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Объём:</label>
                                <div class="col-sm-6">
                                    <input type="text" name="product[volume]" class="form-control" value="<?= @$product['volume']; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include_once 'footer.php'; ?>

