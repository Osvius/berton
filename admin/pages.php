<?php
require_once '../protected/config.php';

if(isset($_POST['pages'])) {
//    $pages = $_POST['pages'];
    setPages($db_connect, $_POST['pages']);
}
    $pages = getPages($db_connect);


?>
<?php include_once 'header.php'; ?>
<body>
    <div id="wrapper" class="toggled">
    <?php include_once 'sidebar.php'; ?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Настроить страницы</h3>
                        <form class="form-horizontal" method="post">
                            <?php
                            foreach ($pages as $row => $page) {
                                ?>
                                <div class="error">
                                    <?php echo (!empty($error))?$error['error']:$success; ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Название кнопки:</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="pages[<?= $row; ?>][button_name]" class="form-control" value="<?= $page['button_name']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Название страницы:</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="pages[<?= $row; ?>][title]" class="form-control" value="<?= $page['title']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Описание:</label>
                                    <div class="col-sm-6">
                                        <textarea cols="20" rows="10" name="pages[<?= $row; ?>][description]" class="form-control"><?= $page['description']; ?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="pages[<?= $row; ?>][page_id]" value="<?= $page['page_id']; ?>"/>
                                </br>
                             <?php } ?>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include_once 'footer.php'; ?>

