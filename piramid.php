<?php
require_once 'protected/config.php';
$page_info = getPage($db_connect, 3);
?>
<!doctype html>
<html lang="ru">
  <head>
    <meta charset="UTF-8" />
    <title>Berton</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.css" integrity="sha256-r2glH03VUY1R1G+7PwWdfL/NaTKpbInwaZFGiohbo4A=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css">
    <link type="text/css" href="css/style.css" rel="stylesheet" media="screen" />
  </head>
<body>
    <div id="fullpage">
        <div class="section leaf-page piramid-page">
            <div class="header">
                <a href="index.php" class="logo">
                    <img src="images/logo.png" alt="logo">
                </a>
            </div>
            <h1>Чай в пирамидках для чашки</h1>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if(isset($page_info['products'])) { ?>
                            <?php foreach($page_info['products'] as $product) { ?>
                                <div class="swiper-slide">
                                    <span class="code"><?= @$product['code']; ?></span>
                                    <img src="<?= SITE_URL . '/images/products/' . @$product['image']; ?>" alt="leaf-tea">
                                    <div class="about-info">
                                        <h2 class="<?= @$product['color']; ?>"><?= @$product['eng_title']; ?></h2>
                                        <h3><?= @$product['rus_title']; ?></h3>
                                        <p>
                                            <?= @$product['product_description']; ?>
                                        </p>
                                        <div class="temp-time">
                                            <?php if(!empty($product['temp'])) { ?>
                                                <span class="temp"><?= $product['temp']; ?>°С</span>
                                            <?php } ?>
                                            <?php if(!empty($product['time'])) { ?>
                                                <span class="time"><?= $product['time']; ?> мин</span>
                                            <?php } ?>
                                            <?php if(!empty($product['volume'])) { ?>
                                                <span class="cup"><?= $product['volume']; ?>мл</span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            <a href="index.php" class="back-link"><i class="fa fa-caret-left" aria-hidden="true"></i> На главную</a>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://use.fontawesome.com/02e4d6d7c6.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.extensions.min.js" integrity="sha256-h15L/g+XIMkwrCo+p9PYghNB2eKEYnjfa1GGxRqLUiY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.js" integrity="sha256-r80qU2FgO31x7HtK2BQxYOQxPb45x+eKKhzA0OrepyM=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#fullpage').fullpage({
                navigation: false,
                scrollingSpeed: 1000,
                keyboardScrolling: true,
                css3: true,
                verticalCentered: false,
                responsiveWidth: 768,
            });
            var swiper = new Swiper('.swiper-container', {
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                paginationClickable: true,
                spaceBetween: 70,
                autoplay: 5000,
                loop: true,
                autoplayDisableOnInteraction: false,
                slidesPerView: 4,
                autoHeight: true,
                breakpoints: {
                    768: {
                        centeredSlides: false,
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    670: {
                        centeredSlides: false,
                        slidesPerView: 2,
                        spaceBetween: 40
                    },
                    420: {
                        slidesPerView: 1,
                        spaceBetween: 50
                    }
                }
            });
        });
    </script>
</body>
</html>