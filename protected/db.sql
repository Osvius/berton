-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.16-0ubuntu0.16.04.1-log - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table berton.main
CREATE TABLE IF NOT EXISTS `main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table berton.main: ~2 rows (approximately)
/*!40000 ALTER TABLE `main` DISABLE KEYS */;
INSERT INTO `main` (`id`, `setting`, `value`) VALUES
	(1, 'admin_name', 'admin'),
	(2, 'admin_pass', 'qwe');
/*!40000 ALTER TABLE `main` ENABLE KEYS */;

-- Dumping structure for table berton.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `button_name` varchar(250) DEFAULT NULL,
  `description` text,
  `title` varchar(250) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table berton.pages: 6 rows
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`page_id`, `button_name`, `description`, `title`, `link`) VALUES
	(1, 'О нас', 'Berton — сбалансированная коллекция листового чая для HoreCa!\r\nОптимальный ассортимент вкусовых предпочтений.\r\nВысокое качество чая, удобство заваривания, оптимальная цена.', 'О нас', NULL),
	(2, 'Leaf tea', 'Отборный ассортимент сортов листового чая тм Berton - оптимальный баланс вкуса и аромата.\r\nКаждый клиент найдет в вашей чайной карте свой любимый сорт чая.', 'Листовой чай', 'leaf_tea'),
	(3, 'Pyramid pack', 'Качество листового чая в фильтрах-пакетах для чашки — наслаждение вкусом при экономии времени заваривания.', 'Чай в пирамидках для чашки', 'piramid'),
	(4, 'Teapot pack', 'Чай расфасованный в фильтр–пакеты для чайника!\r\nУдобство заваривания и оптимальный баланс чая на объём чайника!', 'Чай в фильтр–пакетах для чайника', 'filters'),
	(5, 'Посуда', 'Готовое предложение ассортимента чайной посуды\r\nдля отеля, ресторана и кафе.', 'Посуда', 'dishes'),
	(6, 'Berton в России', 'Представительство компании готово презентовать тм Berton дистрибьюторам направления HoReСa.&lt;br&gt; \r\nСвяжитесь для получения информации c руководителем направления Berton HoReСa&lt;br&gt; \r\nАлексеем Денисовым&lt;br&gt;\r\n                    &lt;div class=&quot;contact-holder&quot;&gt;\r\n                        &lt;a href=&quot;mailto:ADenisov@TeaBerton.com&quot;&gt;ADenisov@TeaBerton.com&lt;/a&gt; \r\n                        &lt;a href=&quot;tel:+74997556393&quot;&gt;+7(499)755-63-93&lt;/a&gt;\r\n                    &lt;/div&gt;', 'Berton в России', NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table berton.products
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `image` text,
  `color` text,
  `eng_title` tinytext,
  `rus_title` tinytext,
  `product_description` text,
  `temperature` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `volume` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table berton.products: ~6 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table berton.product_to_page
CREATE TABLE IF NOT EXISTS `product_to_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Dumping data for table berton.product_to_page: ~15 rows (approximately)
/*!40000 ALTER TABLE `product_to_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_to_page` ENABLE KEYS */;

-- Dumping structure for table berton.route
CREATE TABLE IF NOT EXISTS `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table berton.route: ~0 rows (approximately)
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
/*!40000 ALTER TABLE `route` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
