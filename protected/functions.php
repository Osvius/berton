<?php
function db_connect($host = DB_HOST, $user = DB_USER, $pass = DB_PASS, $name = DB_NAME) {
    $db = mysqli_connect($host, $user, $pass, $name) or die('mysql error');
    $db->query( "SET CHARSET utf8" );
    return $db;
}

$db_connect = db_connect();

function getAdmin($db){
    $query = "SELECT `setting`, `value` FROM main WHERE `setting` IN('admin_name', 'admin_pass')";
    $result = mysqli_query($db, $query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $data;
}

function getPages($db) {
    $query = "SELECT * FROM pages";
    $result = mysqli_query($db, $query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $data;
}

function getPage($db, $page_id) {
    $query = "SELECT title FROM pages WHERE page_id = '" . $page_id . "'";
    $result = mysqli_query($db, $query);
    $data = array();
    $data['page'] = mysqli_fetch_assoc($result);
    $query_2 = "SELECT * FROM products p LEFT JOIN product_to_page ptp ON ptp.product_id = p.product_id WHERE ptp.page_id = '" . $page_id . "'";
    $result_2 = mysqli_query($db, $query_2);
    $data['products'] = mysqli_fetch_all($result_2, MYSQLI_ASSOC);;
    return $data;
}

function getButtons($db) {
    $query = "SELECT button_name FROM pages";
    $result = mysqli_query($db, $query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $buttons = array();
    foreach ($data as $button) {
        $buttons[] = $button['button_name'];
    }
    return $buttons;
}

function setAdminPass($db, $data) {
    $query = "UPDATE main SET `value` = '" . md5(mysqli_real_escape_string($db, $data['new_pass'])) . "' WHERE `setting` = 'admin_pass'";
    mysqli_query($db, $query);
    return true;
}

function setPages($db, $data) {
    foreach ($data as $page) {
        $query = "UPDATE pages SET `button_name` = '" . mysqli_real_escape_string($db, $page['button_name']) . "', `description` = '" . mysqli_real_escape_string($db, htmlspecialchars($page['description'])) . "', `title` = '" . mysqli_real_escape_string($db, $page['title']) . "' WHERE `page_id` = '" . $page['page_id'] . "'";
        mysqli_query($db, $query);
    }
    return true;
}

function getProducts($db) {
    $query = "SELECT * FROM products p INNER JOIN product_to_page ptp ON ptp.product_id = p.product_id LEFT JOIN pages page ON page.page_id = ptp.page_id";
    $result = mysqli_query($db, $query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $data;
}

function getProduct($db, $product_id) {
    $query = "SELECT * FROM products p LEFT JOIN product_to_page ptp ON ptp.product_id = p.product_id LEFT JOIN pages page ON page.page_id = ptp.page_id WHERE p.product_id = '" . $product_id . "'";
    $result = mysqli_query($db, $query);
    $data = mysqli_fetch_assoc($result);
    return $data;
}

function updateProduct($db, $product_id, $product) {
    $query = "UPDATE products SET 
                `rus_title`   = '" . mysqli_real_escape_string($db, $product['rus_title']) . "',
                `eng_title`   = '" . mysqli_real_escape_string($db, $product['eng_title']) . "',
                `code`        = '" . mysqli_real_escape_string($db, $product['code']) . "',
                `image`       = '" . mysqli_real_escape_string($db, $product['image']) . "',
                `color`       = '" . mysqli_real_escape_string($db, $product['color']) . "',
        `product_description` = '" . mysqli_real_escape_string($db, $product['product_description']) . "',
                `temperature` = '" . mysqli_real_escape_string($db, $product['temperature']) . "',
                `time`        = '" . mysqli_real_escape_string($db, $product['time']) . "',
                `volume`      = '" . mysqli_real_escape_string($db, $product['volume']) . "'
                WHERE `product_id` = '" . $product_id . "'";
    mysqli_query($db, $query);
    $query = "UPDATE product_to_page SET `page_id` = '" . $product['page_id'] . "' WHERE `product_id` = '" . $product_id . "'";
    mysqli_query($db, $query);
}

function setProduct($db, $product) {
    $query = "INSERT INTO products SET 
                `rus_title`   = '" . mysqli_real_escape_string($db, $product['rus_title']) . "',
                `eng_title`   = '" . mysqli_real_escape_string($db, $product['eng_title']) . "',
                `code`        = '" . mysqli_real_escape_string($db, $product['code']) . "',
                `image`       = '" . mysqli_real_escape_string($db, $product['image']) . "',
                `color`       = '" . mysqli_real_escape_string($db, $product['color']) . "',
        `product_description` = '" . mysqli_real_escape_string($db, $product['product_description']) . "',
                `temperature` = '" . mysqli_real_escape_string($db, $product['temperature']) . "',
                `time`        = '" . mysqli_real_escape_string($db, $product['time']) . "',
                `volume`      = '" . mysqli_real_escape_string($db, $product['volume']) . "'";
    mysqli_query($db, $query);
    $last_id = mysqli_insert_id($db);
    $query = "INSERT INTO product_to_page SET `page_id` = '" . $product['page_id'] . "', `product_id` = '" . $last_id . "'";
    mysqli_query($db, $query);
}

function deleteProduct($db, $product_id) {
    $query = "DELETE FROM products WHERE product_id = '" . $product_id . "'";
    mysqli_query($db, $query);
    $query = "DELETE FROM product_to_page WHERE product_id = '" . $product_id . "'";
    mysqli_query($db, $query);

}