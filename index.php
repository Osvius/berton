<?php
require_once 'protected/config.php';
$pages = getPages($db_connect);
$buttons = getButtons($db_connect);
?>
<!doctype html>
<html lang="ru">
  <head>
    <meta charset="UTF-8" />
    <title>Berton</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.css" integrity="sha256-r2glH03VUY1R1G+7PwWdfL/NaTKpbInwaZFGiohbo4A=" crossorigin="anonymous" />
    <link type="text/css" href="css/style.css" rel="stylesheet" media="screen" />
    <script src="https://use.fontawesome.com/02e4d6d7c6.js"></script>
  </head>
<body>
    <div id="fullpage">
        <div class="section page1">
            <div class="header">
                <a href="index.php" class="logo">
                    <img src="images/logo.png">
                </a>
            </div>
            <h1>Добро пожаловать на презентацию торговой марки<br>Berton</h1>
            <!-- <img class="mouse" src="images/mouse.png" alt="scroll-down"> -->
        </div>
        <?php $section = 2; ?>
        <?php foreach ($pages as $page) { ?>
        <div class="section page<?= $section; ?>">
            <div class="description">
                <h2>
                    <?= htmlspecialchars_decode($page['description']); ?>
                    <?php
                    if(!empty($page['link'])) {
                        echo '<a href="' . SITE_URL . '/' . $page['link'] . '.php" class="link_button">Подробнее</a>';
                    }
                    ?>
                </h2>
            </div>
        </div>
        <?php $section++; ?>
        <?php } ?>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.extensions.min.js" integrity="sha256-h15L/g+XIMkwrCo+p9PYghNB2eKEYnjfa1GGxRqLUiY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.js" integrity="sha256-r80qU2FgO31x7HtK2BQxYOQxPb45x+eKKhzA0OrepyM=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var buttons = [''];
            <?php foreach ($buttons as $button) { ?>
                buttons.push("<?php echo $button; ?>");
            <?php } ?>
            $('#fullpage').fullpage({
                navigation: true,
                scrollingSpeed: 1000,
                keyboardScrolling: true,
                css3: true,
                verticalCentered: false,
                responsiveWidth: 768,
                fixedElements: '.header',
                navigationPosition: ['left'],
                navigationTooltips: buttons//['','О нас','Leaf tea','Pyramid pack','Teapot pack','Посуда','Berton в России']
            });
        });
    </script>
</body>
</html>